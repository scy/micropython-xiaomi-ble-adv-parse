from xiaomi_ble_adv_parse import parse_adv_data


print(parse_adv_data(memoryview(bytes.fromhex("020106121695fe5020aa013a7806dca8654c0a100124"))))
print(parse_adv_data(memoryview(bytes.fromhex("020106131695fe5020aa01d67806dca8654c0610024702"))))
print(parse_adv_data(memoryview(bytes.fromhex("020106131695fe5020aa012f7806dca8654c041002e700"))))
print(parse_adv_data(memoryview(bytes.fromhex("020106151695fe5020aa019e7806dca8654c0d10041601b901"))))
